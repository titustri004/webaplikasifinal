<?php

// ambil data
$namaFile = $_FILES['berkas']['name'];
$namaSementara = $_FILES['berkas']['tmp_name'];

// lokasi file
$dirUpload = 'upload/';

// pindah file
$terupload = move_uploaded_file($namaSementara, $dirUpload.$namaFile);

?>
<html>
    <head>
        <title>Upload Jawaban</title>
    </head>
    <body>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
      <div class="container mt-5">
        <div class="card">
          <div class="card-body">
            <div class="mb-3">
            <?php
            if ($terupload) {
                echo '<div class="alert alert-success" role="alert">
                <div class="alert-message">
                Upload Berhasil
                </div>
              </div>';
              echo '<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
              Lihat file 
            </button>
            <a href="index.html"><button class="btn btn-secondary">Kembali ke Halaman Utama</button></a>
            ';
            } else {
                echo '<div class="alert alert-danger" role="alert">
                <div class="alert-message">
                  Upload Gagal !!!
                </div>
              </div>';
            }
            ?>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?php echo $namaFile; ?></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                  <?php echo file_get_contents( $dirUpload.$namaFile ); ?>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
      
      
    </body>
</html> 